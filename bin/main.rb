# frozen_string_literal: true

require 'rubygems'
require 'bundler/setup'

# require 'pry'

require 'logger'
# require 'optparse'

Dir['./lib/**/*.rb'].sort.each { |f| require f }

logger = Logger.new(STDOUT)
logger.level = ENV.fetch('LOG_LEVEL') { :debug }.to_sym

FILE_LOCATION = 'vendor/data/test-data-10-exp-5.list'

# Handle Exit
at_exit do
  logger.debug { 'Name parser completed.' }
end

# Outputs to STDOUT
begin
  file = File.open(FILE_LOCATION)
  person = Person.new(file, logger: logger)
  names = person.parse_names

  output = <<~TEXT
    Question #1\n
    There are #{names[:full].size} unique full names.
    There are #{names[:first].size} unique first names.
    There are #{names[:last].size} unique last names.
  TEXT

  $stdout.puts(output)

  first_names = person.top_names(type: :first)
  $stdout.puts("\nQuestion #2")
  $stdout.puts(first_names)

  last_names = person.top_names(type: :last)
  $stdout.puts("\nQuestion #3")
  $stdout.puts(last_names)
rescue StandardError => e
  logger.error(error: 'An unexpected error occurred', message: e.message, backtrace: e.backtrace)
end
