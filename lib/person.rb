# frozen_string_literal: true

class Person
  attr_reader :file, :logger
  attr_accessor :unique_names

  def initialize(file, logger: Logger.new(STDOUT))
    @file = file
    @logger = logger
    @unique_names ||= {
      full: {},
      first: {},
      sorted_first: {},
      last: {},
      sorted_last: {}
    }
  end

  # @param file [File] file to parse
  # @return [Hash]
  def parse_names
    file.each do |row|
      name = parse_name(row)
      next if name.nil? || name.empty?

      (unique_names[:full][name.join('_').to_sym] ||= []) << name
      (unique_names[:first][name.last.to_sym] ||= []) << name
      (unique_names[:last][name.first.to_sym] ||= []) << name
    end

    # order by size decending
    unique_names[:sorted_first] = unique_names[:first].sort_by { |_key, value| -value.size }
    unique_names[:sorted_last] = unique_names[:last].sort_by { |_key, value| -value.size }
    unique_names
  end

  # @param type [Symbol]
  # @param count [Integer]
  def top_names(type:, count: 10)
    list = unique_names["sorted_#{type}".to_sym].first(count)

    output = ["The ten most common #{type} names are:"]

    list.each do |key, value|
      output << "#{key} (#{value.count})"
    end

    output.join("\n\t")
  end

  private

  # @param row [String]
  # @return Array[String] Last name, First name
  def parse_name(row)
    return if row[/\s\s(.*)/] # this assumes that non-name lines start with double spaces

    name = row[/(.*)\s--/, 1] # this assumes that the format is Last, First with double hyphens
    # logger.debug(row: row, name: name)

    name.split(',').map!(&:strip)
  end
end
