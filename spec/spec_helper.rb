# frozen_string_literal: true

require 'pry'
require 'simplecov'

SimpleCov.start do
  add_filter '/spec/'
end

Dir['./lib/**/*.rb'].sort.each { |f| require f }
Dir['./spec/support/**/*.rb'].sort.each { |f| require f }

ENV['RACK_ENV'] = 'test'
ENV['LOG_LEVEL'] = 'error'

RSpec.configure do |config|
  config.order = :random

  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end
